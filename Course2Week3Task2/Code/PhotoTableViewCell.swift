//
//  PhotoTableViewCell.swift
//  Course2Week3Task2
//
//  Created by Emperor on 02.05.2018.
//  Copyright © 2018 e-Legion. All rights reserved.
//

import UIKit

class PhotoTableViewCell: UITableViewCell {
    
    
    @IBOutlet  weak var photoView: UIImageView!
    @IBOutlet  weak var photoNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
//        photoView.frame.origin = CGPoint(x: 0, y: 0)
//        photoView.frame.size   = CGSize(width: 180, height: 60)
//        
//        photoNameLabel.frame.origin = CGPoint(x: 188, y: 20)
    }
    
    func setCellDetails(withImage image: UIImage, andDescription description: String) {
        photoView.image = image
        photoNameLabel.text = description
    }
    
}
